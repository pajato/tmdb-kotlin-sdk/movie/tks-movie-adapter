plugins {
    alias(libs.plugins.kmp.lib)
}

group = "com.pajato.tks"
version = "0.10.7"
description = "The TMDb Kotlin Sdk (tks) movie feature, interface adapters layer, KMP common target project"

kotlin.sourceSets {
    val commonMain by getting {
        dependencies {
            implementation(libs.kotlinx.coroutines.core)
            implementation(libs.kotlinx.serialization.json)

            implementation(libs.tks.common.core)
            implementation(libs.tks.common.adapter)
            implementation(libs.tks.movie.core)
        }
    }

    val commonTest by getting {
        dependencies {
            implementation(libs.kotlin.test)
            implementation(libs.pajato.test)
        }
    }
}
