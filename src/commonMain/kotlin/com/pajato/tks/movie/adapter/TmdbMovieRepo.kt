package com.pajato.tks.movie.adapter

import com.pajato.tks.common.adapter.Strategy
import com.pajato.tks.common.adapter.TmdbFetcher
import com.pajato.tks.common.core.MovieKey
import com.pajato.tks.movie.core.Movie
import com.pajato.tks.movie.core.MovieRepo

/**
 * TmdbMovieRepo is an implementation of the MovieRepo interface.
 * It provides methods to interact with the TMDb API for retrieving movie data.
 */
public object TmdbMovieRepo : MovieRepo {

    /**
     * Fetches a Movie object from the TMDb service based on the provided MovieKey.
     *
     * @param key The MovieKey containing the ID of the movie to be retrieved.
     * @return The fetched Movie object.
     */
    override suspend fun getMovie(key: MovieKey): Movie {
        val path = "movie/${key.id}"
        val queryParams: List<String> = listOf()
        val serializer: Strategy<Movie> = Movie.serializer()
        return TmdbFetcher.fetch(path, queryParams, key, serializer, Movie())
    }
}
