package com.pajato.tks.movie.adapter

import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.adapter.TmdbApiService.TMDB_BASE_API3_URL
import com.pajato.tks.common.adapter.TmdbFetcher
import com.pajato.tks.common.core.InjectError
import com.pajato.tks.common.core.MovieKey
import com.pajato.tks.common.core.jsonFormat
import com.pajato.tks.movie.adapter.TmdbMovieRepo.getMovie
import com.pajato.tks.movie.core.Movie
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.fail

class MovieRepoUnitTest : ReportingTestProfiler() {
    private val apiKey = "8G9B0WX31T2U7P0Q"
    private val id = 653851
    private val key = MovieKey(id)
    private val url = "$TMDB_BASE_API3_URL/movie/$id?api_key=$apiKey"
    private val resourceName = "devotion_video.json"
    private val urlConverterMap: Map<String, String> = mapOf(url to resourceName)

    private lateinit var errorMessage: String
    private lateinit var errorExc: String
    private lateinit var errorExcMessage: String

    @BeforeTest fun setUp() {
        TmdbFetcher.inject(::fetch, ::handleError)
        TmdbFetcher.inject(apiKey)
        errorMessage = ""
        errorExc = ""
        errorExcMessage = ""
    }

    @Test fun `When accessing a cached movie, verify behavior`() {
        val movie: Movie = jsonFormat.decodeFromString(getJson(resourceName))
        runBlocking { assertEquals(movie, getMovie(key)) }
    }

    @Test fun `When accessing a non-cached Movie show, verify behavior`() {
        fun getExpectedMovie(): Movie = jsonFormat.decodeFromString(getJson(resourceName))
        runBlocking { assertEquals(getExpectedMovie(), getMovie(key)) }
    }

    @Test fun `When simulating a URL fetch without injection, verify behavior`() {
        val expected = "No api key has been injected!: No repo implementation has been injected!"
        TmdbFetcher.reset()
        runBlocking { assertFailsWith<InjectError> { getMovie(key) }.also { assertEquals(expected, it.message) } }
    }

    private fun fetch(key: String): String {
        val name = urlConverterMap[key] ?: fail("Key $key is not mapped to a test resource name!")
        return getJson(name).ifEmpty { throw IllegalStateException("No JSON available!") }
    }

    private fun getJson(name: String): String = javaClass.classLoader.getResource(name)?.readText() ?: ""

    private fun handleError(message: String, exc: Exception?) {
        errorMessage = message
        errorExc = if (exc == null) "null" else exc.javaClass.name
        errorExcMessage = if (exc == null) "null" else exc.message ?: "null"
    }
}
